<html>
<head>
<link rel="stylesheet" href="external.css">
</head>
<body>
<h1> selectors in css</h1>
<p class="p1">CSS selectors are used to "find" (or select) the HTML
elements we want to style.
We can divide CSS selectors into five categories:
<ul>
<li>Simple selectors</li>
<li>Combinator selectors</li>
<li>Pseudo-class selectors</li>
<li>Pseudo-elements selectors</li>
<li>Attribute selectors</li>
</ul></p>
<h2>simple selector</h2><br><p class="p2">
The id selector uses the id attribute of an HTML element to select a specific element.
The id of an element is unique within a page, so the id selector is used to select one unique element!
To select an element with a specific id, write a hash (#) character, followed by the id of the element.</p>
<h2>Combinator selectors</h2><br><p class="p3">
A CSS selector can contain more than one simple selector. Between the simple selectors, we can include a combinator.There are four different combinators in CSS:
descendant selector (space)
child selector (>)
adjacent sibling selector (+)
general sibling selector (~)</p>
<h2>Pseudo-class selectors</h2><br><p class="p4">
A pseudo-class is used to define a special state of an element.
For example, it can be used to:
Style an element when a user mouses over it
Style visited and unvisited links differently
Style an element when it gets focus</p>
<h2>Pseudo-class selectors</h2><br><p class="p5">
A CSS pseudo-element is used to style specified parts of an element.
For example, it can be used to:
Style the first letter, or line, of an element
Insert content before, or after, the content of an elementM</p>
<h2>Attribute selectors</h2><br><p class="p6">
The [attribute] selector is used to select elements with a specified attribute.
The following example selects all <a> elements with a target attribute:</p>
</body>
</html> 
